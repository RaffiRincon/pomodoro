//
//  NotificationViewController.swift
//  CountdownNotificationContentExtension
//
//  Created by Rafael Rincon on 8/30/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: TimeDisplayViewController, UNNotificationContentExtension {
    
    private let doneLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureTimeDisplay()
        configureDoneLabel()
    }
    
    private func configureView() {
        preferredContentSize.height = K.Dimensions.Notification.height
        view.backgroundColor = K.Color.background
    }
    
    private func configureTimeDisplay() {
        timeDisplay.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(timeDisplay)
        NSLayoutConstraint.activate([
            timeDisplay.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            timeDisplay.centerYAnchor.constraint(equalTo: view.centerYAnchor)
            ])
    }
    
    private func configureDoneLabel() {
        doneLabel.translatesAutoresizingMaskIntoConstraints = false
        doneLabel.isHidden = true
        
        doneLabel.font = K.Font.notificationDoneLabel
        doneLabel.textColor = .white
        
        view.addSubview(doneLabel)
        NSLayoutConstraint.activate([
            doneLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            doneLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor)
            ])
    }
    
    private func showDoneLabel(for sessionType: SessionCategory) {
        timeDisplay.isHidden = true
        doneLabel.isHidden = false
        switch sessionType {
        case .pomodoro:
            doneLabel.text = K.String.Notification.pomodoroDone
        case .break:
            doneLabel.text = K.String.Notification.breakDone
        }
    }
    
    override func startPomodoro(dateStarted: Date) {
        timeDisplay.configureColorsForPomodoro()
        super.startPomodoro(dateStarted: dateStarted)
    }
    
    override func startBreak(dateStarted: Date) {
        timeDisplay.configureColorsForBreak()
        super.startBreak(dateStarted: dateStarted)
    }
    
    func didReceive(_ notification: UNNotification) {
        let userInfo = notification.request.content.userInfo
        
        let event = SessionEvent(rawValue: userInfo[K.String.Notification.UserInfoKey.event] as! String)!
        let category = SessionCategory(rawValue: userInfo[K.String.Notification.UserInfoKey.category] as! String)!
        let date = userInfo[K.String.Notification.UserInfoKey.date] as? Date
        let secondsLeft = userInfo[K.String.Notification.UserInfoKey.secondsLeft] as? Int
        let timeToNextFiringWhenPaused = userInfo[K.String.Notification.UserInfoKey.timeToNextFiringWhenPaused] as? TimeInterval
        
        switch (event, category) {
        case (.start, .pomodoro):
            startPomodoro(dateStarted: date!)
        case (.start, .break):
            startBreak(dateStarted: date!)
        case (.resume, .break):
            getReadyForBreak()
            resume(dateResumed: date!, secondsLeftBeforeResuming: secondsLeft!, timeToNextFiringWhenPaused: timeToNextFiringWhenPaused!)
        case (.resume, .pomodoro):
            getReadyForPomodoro()
            resume(dateResumed: date!, secondsLeftBeforeResuming: secondsLeft!, timeToNextFiringWhenPaused: timeToNextFiringWhenPaused!)
        case (.pause, .break):
            getReadyForBreak()
            timeDisplay.setTimeLabelTexts(forSecondsLeft: secondsLeft!)
        case (.pause, .pomodoro):
            getReadyForPomodoro()
            timeDisplay.setTimeLabelTexts(forSecondsLeft: secondsLeft!)
        case (.finish, _):
            showDoneLabel(for: category)
        }
    }
}
