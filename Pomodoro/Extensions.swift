//
//  Extensions.swift
//  Pomodoro
//
//  Created by Rafael Rincon on 9/8/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import UIKit

extension UIView {
    func addShadow(forHeight height: CGFloat) {
        layer.shadowOffset = CGSize(width: 0, height: height)
        layer.shadowRadius = K.Dimensions.Shadow.blurRadius
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = K.Color.Shadow.alpha
    }
}

extension UIButton {
    static func iconButton() -> UIButton {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        let radius = K.Dimensions.IconButton.radius
        
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: radius * 2),
            button.heightAnchor.constraint(equalToConstant: radius * 2)
            ])
    
        button.tintColor = .white
        button.titleLabel?.font = K.Font.IconButton.font
        button.adjustsImageWhenHighlighted = true
        
        return button
    }
    
    /**
     Creates a button with a rounded (circular) outline and a light background.
     Adds height and width constraints.
     */
    static func outlinedButton(width: CGFloat, height: CGFloat) -> UIButton {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: width),
            button.heightAnchor.constraint(equalToConstant: height)
            ])
        
        button.layer.cornerRadius = height / 2
        
        button.layer.borderWidth = 2
        button.layer.borderColor = K.Color.Button.outline.cgColor
        button.backgroundColor = K.Color.Button.lightBackground
        
        return button
    }
}

extension UIColor {
    static func with(red: Int, green: Int, blue: Int) -> UIColor {
        return UIColor(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1)
    }
}

extension UIViewController {
    
    func presentOKCancelAlertController(title: String, message: String, okAction: (()->Void)? = nil) {
        let controller = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            okAction?()
        })
        controller.addAction(okAction)
        
        self.present(controller, animated: true, completion: nil)
    }
}
