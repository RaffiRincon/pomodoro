//
//  AppDelegate.swift
//  Pomodoro
//
//  Created by Rafael Rincon on 8/16/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        UNUserNotificationCenter.current().delegate = NotificationManager.shared
        
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // set up AdMob
        GADMobileAds.configure(withApplicationID: K.String.AdMob.appID)
        
        window = UIWindow()
        
        let initialViewController = ManageTasksViewController()
        let navigationController = UINavigationController(rootViewController: initialViewController)
        navigationController.navigationBar.backgroundColor = K.Color.navigationBar
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        requestPermissionForNotificationIfNeeded(viewController: window!.rootViewController!)
        
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        saveContext()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        if let viewTaskViewController = (window?.rootViewController as? UINavigationController)?.visibleViewController as? ViewTaskViewController {
            viewTaskViewController.handleApplicationEnteringBackground()
        }
        saveContext()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        if let viewTaskViewController = (window?.rootViewController as? UINavigationController)?.visibleViewController as? ViewTaskViewController {
            viewTaskViewController.handleApplicationEnteringForeground()
        }
    }
    
    // MARK: - Permissions
    
    private func requestPermissionForNotificationIfNeeded(viewController: UIViewController) {
        UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (notificationSettings: UNNotificationSettings) in
            let shouldNotAskAgain = UserDefaults().value(forKey: K.String.UserDefaults.doNotAskAgainKey) as? Bool
            
            if shouldNotAskAgain != nil && shouldNotAskAgain! == false {
                return
            }
            
            guard notificationSettings.alertSetting != .enabled else {
                return
            }
            
            let alertTitle =  K.String.AlertController.NotificationPermissionRequest.title
            let alertMessage = K.String.AlertController.NotificationPermissionRequest.message
            let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
            
            let cancelTitle = K.String.AlertController.NotificationPermissionRequest.leftButtonTitle
            let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let doNotAskAgainTitle = K.String.AlertController.NotificationPermissionRequest.middleButtonTitle
            let doNotAskAgainAction = UIAlertAction(title: doNotAskAgainTitle, style: .default, handler: { (action) in
                UserDefaults().setValue(false, forKey: K.String.UserDefaults.doNotAskAgainKey)
            })
            
            alertController.addAction(doNotAskAgainAction)
            
            let allowTitle = K.String.AlertController.NotificationPermissionRequest.rightButtonTitle
            let allowAction = UIAlertAction(title: allowTitle, style: .default) { (action) in
                let center = UNUserNotificationCenter.current()
                center.requestAuthorization(options: [.sound, .alert], completionHandler: { (granted, error) in
                    if !granted {
                        let goIntoSettingsAlert = UIAlertController(
                            title: K.String.AlertController.GoIntoSettingsForNotifications.title,
                            message: K.String.AlertController.GoIntoSettingsForNotifications.message,
                            preferredStyle: .alert)
                        
                        let goToSettingsAction = UIAlertAction(title: K.String.AlertController.GoIntoSettingsForNotifications.goButtonTitle, style: .default, handler: { (action) in
                            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                return
                            }
                            
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                UIApplication.shared.open(settingsUrl)
                            }
                        })
                        goIntoSettingsAlert.addAction(goToSettingsAction)
                        
                        let cancel = UIAlertAction(
                            title: K.String.AlertController.GoIntoSettingsForNotifications.cancelButtonTitle,
                            style: .cancel,
                            handler: nil)
                        goIntoSettingsAlert.addAction(cancel)
                        
                        viewController.present(goIntoSettingsAlert, animated: true)
                    }
                })
            }
            alertController.addAction(allowAction)
            
            viewController.present(alertController, animated: true)
        })
    }
    
    // MARK: - Core Data stack
    
    static var persistentContainer: NSPersistentContainer {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer
    }
    
    static var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                
            }
        }
    }
}

