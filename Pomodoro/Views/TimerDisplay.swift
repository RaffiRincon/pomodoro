//
//  TimerView.swift
//  Pomodoro
//
//  Created by Rafael Rincon on 8/30/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import UIKit

class TimeDisplay: UIView {
    
    private let tensOfMinutesLabel = UILabel()
    private let minutesLabel = UILabel()
    private let tensOfSecondsLabel = UILabel()
    private let secondsLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSelf()
        configureLabels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - View Configuration
    
    private func configureSelf() {
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: K.Dimensions.TimerView.width),
            heightAnchor.constraint(equalToConstant: K.Dimensions.TimerView.height)
            ])
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 5
    }
    
    private func configureLabels() {
        
        let tensOfMinutesLabelRightAnchor = rightAnchorAfterConfiguringTimerLabel(
            tensOfMinutesLabel,
            width: K.Dimensions.TimerView.numberLabelWidth,
            inContainer: self,
            leftAnchor: leftAnchor,
            leftSpacing: K.Spacing.small)
        
        let minutesLabelRightAnchor = rightAnchorAfterConfiguringTimerLabel(
            minutesLabel,
            width: K.Dimensions.TimerView.numberLabelWidth,
            inContainer: self,
            leftAnchor: tensOfMinutesLabelRightAnchor,
            leftSpacing: 0)
        
        let tensOfSecondsLabelRightAnchor = rightAnchorAfterConfiguringTimerLabel(
            tensOfSecondsLabel,
            width: K.Dimensions.TimerView.numberLabelWidth,
            inContainer: self,
            leftAnchor: minutesLabelRightAnchor,
            leftSpacing: K.Spacing.large)
        
        let _ = rightAnchorAfterConfiguringTimerLabel(
            secondsLabel,
            width: K.Dimensions.TimerView.numberLabelWidth,
            inContainer: self,
            leftAnchor: tensOfSecondsLabelRightAnchor,
            leftSpacing: 0)
    }
    
    private func rightAnchorAfterConfiguringTimerLabel(
        _ timerLabel: UILabel,
        width: CGFloat,
        inContainer containerView: UIView,
        leftAnchor: NSLayoutXAxisAnchor,
        leftSpacing: CGFloat) -> NSLayoutXAxisAnchor {
        
        timerLabel.translatesAutoresizingMaskIntoConstraints = false
        timerLabel.textAlignment = .center
        timerLabel.font = K.Font.TimerLabel.font
        timerLabel.textColor = .white
        
        containerView.addSubview(timerLabel)
        NSLayoutConstraint.activate([
            timerLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: leftSpacing),
            timerLabel.widthAnchor.constraint(equalToConstant: width),
            timerLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
            ])
        
        return timerLabel.rightAnchor
    }
    
    func configureColorsForPomodoro() {
        setLabelFontColor(K.Color.background)
        backgroundColor = UIColor.white
    }
    
    func configureColorsForBreak() {
        setLabelFontColor(.white)
        backgroundColor = K.Color.background
    }
    
    private func setLabelFontColor(_ color: UIColor) {
        minutesLabel.textColor = color
        tensOfMinutesLabel.textColor = color
        secondsLabel.textColor = color
        tensOfSecondsLabel.textColor = color
    }
    
    // MARK: - Miscellaneous
    
    @objc func setTimeLabelTexts(forSecondsLeft secondsLeft: Int) {
        let minutesTotal = secondsLeft / 60
        let tensOfMinutes = minutesTotal / 10
        let singleMinutes = minutesTotal % 10
        
        let secondsTotal = secondsLeft % 60
        let tensOfSeconds = secondsTotal / 10
        let singleSeconds = secondsTotal % 10
        
        guard tensOfMinutes >= 0,
            singleMinutes >= 0,
            tensOfSeconds >= 0,
            singleSeconds >= 0 else {
            return
        }
        
        tensOfMinutesLabel.text = "\(tensOfMinutes)"
        minutesLabel.text = "\(singleMinutes)"
        tensOfSecondsLabel.text = "\(tensOfSeconds)"
        secondsLabel.text = "\(singleSeconds)"
    }
}
