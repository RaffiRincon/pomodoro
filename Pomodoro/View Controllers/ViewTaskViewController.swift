//
//  ViewController.swift
//  Pomodoro
//
//  Created by Rafael Rincon on 8/16/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class ViewTaskViewController: TimeDisplayViewController {
    
    var task: Task?
    private var pomodorosCompletedWithoutTask = Int(0)
    private var enteredBackgroundDate: Date?
    private var timeLeftWhenEnteredBackground: TimeInterval?
    
    private let bannerAd = GADBannerView(adSize: GADAdSize.init(
        size: K.Dimensions.ViewPomodoroBannerAd.size,
        flags: 0))
    private let completedLabel = UILabel()
    
    private let startPauseResumeButton = UIButton.iconButton()
    private let skipButton = UIButton.iconButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SessionStateManager.shared.prepareForFirstSession()
        
        configureView()
        configureNavigationItem()
        configuretimeDisplay()
        configureBannerAdAndCompletedLabel()
        
        configureButtons()
        
        getReadyForPomodoro()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bannerAd.load(GADRequest())
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isIdleTimerDisabled = false
        NotificationManager.shared.removeAllNotifications()
    }
    
    // MARK: - Configure Views
    
    private func configureBannerAdAndCompletedLabel() {
        bannerAd.translatesAutoresizingMaskIntoConstraints = false
        bannerAd.rootViewController = self
        bannerAd.adUnitID = K.String.AdMob.viewPomodoroBannerAdUnitID
        
        view.addSubview(bannerAd)
        
        NSLayoutConstraint.activate([
            bannerAd.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            bannerAd.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            ])
        
        completedLabel.translatesAutoresizingMaskIntoConstraints = false
        completedLabel.font = K.Font.completedLabel
        setCompletedLabelText()
        completedLabel.textAlignment = .center
        completedLabel.textColor = .white
        
        view.addSubview(completedLabel)
        NSLayoutConstraint.activate([
            completedLabel.bottomAnchor.constraint(equalTo: bannerAd.topAnchor, constant: -K.Spacing.small),
            completedLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
            ])
    }
    
    private func configureView() {
        view.backgroundColor = K.Color.background
    }
    
    private func configureNavigationItem() {
        guard task != nil else {
            return
        }
        
        navigationItem.title = task!.title
    }
    
    private func configuretimeDisplay() {
        timeDisplay.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(timeDisplay)
        NSLayoutConstraint.activate([
            timeDisplay.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            timeDisplay.bottomAnchor.constraint(equalTo: view.centerYAnchor),
            ])
    }
    
    private func rightAnchorAfterConfiguringTimerLabel(
        _ timerLabel: UILabel,
        width: CGFloat,
        inContainer containerView: UIView,
        leftAnchor: NSLayoutXAxisAnchor,
        leftSpacing: CGFloat) -> NSLayoutXAxisAnchor {
        
        timerLabel.translatesAutoresizingMaskIntoConstraints = false
        timerLabel.textAlignment = .center
        timerLabel.font = K.Font.TimerLabel.font
        timerLabel.textColor = .white
        
        containerView.addSubview(timerLabel)
        NSLayoutConstraint.activate([
            timerLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: leftSpacing),
            timerLabel.widthAnchor.constraint(equalToConstant: width),
            timerLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
            ])
        
        return timerLabel.rightAnchor
    }
    
    private func setStartPauseResumeButtonImageForCurrentState() {
        switch SessionStateManager.shared.state {
        case .breakReady, .pomodoroReady:
            startPauseResumeButton.setImage(UIImage(imageLiteralResourceName: "start"), for: .normal)
        case .pomodoroPaused, .breakPaused:
            startPauseResumeButton.setImage(UIImage(imageLiteralResourceName: "resume"), for: .normal)
        case .breakRunning, .pomodoroRunning:
            startPauseResumeButton.setImage(UIImage(imageLiteralResourceName: "pause"), for: .normal)
        }
    }
    
    private func configureButtons() {
        
        // configure startPauseResumeButton
        setStartPauseResumeButtonImageForCurrentState()
        startPauseResumeButton.addTarget(self, action: #selector(handleStartPauseResumeButtonTap(button:)), for: .touchUpInside)
        startPauseResumeButton.translatesAutoresizingMaskIntoConstraints = false
        
        // lay out timeDisplay to use width for placement of buttons
        timeDisplay.layoutIfNeeded()
        
        view.addSubview(startPauseResumeButton)
        NSLayoutConstraint.activate([
            startPauseResumeButton.centerXAnchor.constraint(equalTo: timeDisplay.leftAnchor, constant: timeDisplay.frame.width * 0.25),
            startPauseResumeButton.topAnchor.constraint(equalTo: timeDisplay.bottomAnchor, constant: K.Spacing.large)
            ])
        
        // configure skipButton
        skipButton.setImage(#imageLiteral(resourceName: "skip"), for: .normal)
        skipButton.addTarget(self, action: #selector(skip), for: .touchUpInside)
        skipButton.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(skipButton)
        NSLayoutConstraint.activate([
            skipButton.centerXAnchor.constraint(equalTo: timeDisplay.rightAnchor, constant: timeDisplay.frame.width * -0.25),
            skipButton.topAnchor.constraint(equalTo: timeDisplay.bottomAnchor, constant: K.Spacing.large)
            ])
    }
    
    // MARK: - Update Labels
    
    private func setCompletedLabelText() {
        guard task != nil else {
            completedLabel.text = "Completed: \(pomodorosCompletedWithoutTask)"
            return
        }
        
        completedLabel.text = Task.textFromTask(task!)
    }
    
    // MARK: - Button
    
    @objc private func handleStartPauseResumeButtonTap(button: UIButton) {
        button.isEnabled = false
        let date = Date()
        SessionStateManager.shared.updateStateForStartPauseResume()
        switch SessionStateManager.shared.previousState {
        case .breakRunning, .pomodoroRunning:
            pause(datePaused: date)
        case .breakPaused, .pomodoroPaused:
            resume(dateResumed: date, secondsLeftBeforeResuming: secondsLeft, timeToNextFiringWhenPaused: timeToNextFiringWhenPaused)
        case .pomodoroReady:
            startPomodoro(dateStarted: date)
        case .breakReady:
            startBreak(dateStarted: date)
        }
        
        setStartPauseResumeButtonImageForCurrentState()
        
        button.isEnabled = true
    }
    
    // MARK: Timer
    
    override func startPomodoro(dateStarted: Date) {
        super.startPomodoro(dateStarted: dateStarted)
        UIApplication.shared.isIdleTimerDisabled = true
        NotificationManager.shared.sendStartPomodoroNotification(dateStarted: dateStarted)
    }
    
    override func startBreak(dateStarted: Date) {
        super.startBreak(dateStarted: dateStarted)
        UIApplication.shared.isIdleTimerDisabled = true
        NotificationManager.shared.sendStartBreakNotification(dateStarted: dateStarted)
    }
    
    override func pause(datePaused: Date) {
        super.pause(datePaused: datePaused)
        UIApplication.shared.isIdleTimerDisabled = false
        
        switch SessionStateManager.shared.state.type {
        case .break:
            NotificationManager.shared.sendPauseBreakNotification(secondsLeft: secondsLeft, datePaused: datePaused)
        case .pomodoro:
            NotificationManager.shared.sendPausePomodoroNotification(secondsLeft: secondsLeft, datePaused: datePaused)
        }
    }
    
    override func resume(dateResumed: Date, secondsLeftBeforeResuming: Int, timeToNextFiringWhenPaused: TimeInterval) {
        super.resume(dateResumed: dateResumed, secondsLeftBeforeResuming: secondsLeftBeforeResuming, timeToNextFiringWhenPaused: timeToNextFiringWhenPaused)
        
        switch SessionStateManager.shared.state.type {
        case .break:
            NotificationManager.shared.sendResumeBreakNotification(
                dateResumed: dateResumed,
                secondsLeft: secondsLeftBeforeResuming,
                timeToNextFiringWhenPaused: timeToNextFiringWhenPaused)
        case .pomodoro:
            NotificationManager.shared.sendResumePomodoroNotification(
                dateResumed: dateResumed,
                secondsLeft: secondsLeftBeforeResuming,
                timeToNextFiringWhenPaused: timeToNextFiringWhenPaused)
        }
    }
    
    @objc func skip() {
        SessionStateManager.shared.updateStateForSkip()
        
        switch SessionStateManager.shared.previousState.type {
        case .pomodoro:
            getReadyForBreak()
        case .break:
            getReadyForPomodoro()
        }
        
        setStartPauseResumeButtonImageForCurrentState()
        NotificationManager.shared.removeAllNotifications()
    }
    
    override func handleSessionEnding() {
        super.handleSessionEnding()
        UIApplication.shared.isIdleTimerDisabled = false
        SessionStateManager.shared.updateStateForEndOfSession()
        setStartPauseResumeButtonImageForCurrentState()
        
        if SessionStateManager.shared.previousState.type == .pomodoro {
            if task == nil {
                pomodorosCompletedWithoutTask += 1
            } else {
                task!.numberOfPomodorosDone += 1
            }
            
            setCompletedLabelText()
        }
    }
    
    override func getReadyForBreak() {
        super.getReadyForBreak()
        setStartPauseResumeButtonImageForCurrentState()
    }
    
    override func getReadyForPomodoro() {
        super.getReadyForPomodoro()
        setStartPauseResumeButtonImageForCurrentState()
    }
    
    // application lifecycle events
    
    func handleApplicationEnteringBackground() {
        guard SessionStateManager.shared.state == .pomodoroRunning
            || SessionStateManager.shared.state == .breakRunning,
            sessionTimer != nil else {
                return
        }
        enteredBackgroundDate = Date()
        timeLeftWhenEnteredBackground = sessionTimer!.fireDate.timeIntervalSinceNow + TimeInterval(secondsLeft)
        sessionTimer?.invalidate()
    }
    
    func handleApplicationEnteringForeground() {
        guard SessionStateManager.shared.state == .pomodoroRunning
            || SessionStateManager.shared.state == .breakRunning,
            timeLeftWhenEnteredBackground != nil,
            enteredBackgroundDate != nil else {
                return
        }
        
        let elapsedTime = Date().timeIntervalSince(enteredBackgroundDate!)
        
        let timeLeft = timeLeftWhenEnteredBackground! - elapsedTime
        
        secondsLeft = Int(timeLeft)
        let delay = timeLeft - Double(secondsLeft)
        startTimer(delay: delay)
    }
}

