//
//  TimerViewController.swift
//  Pomodoro
//
//  Created by Rafael Rincon on 9/18/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import UIKit

/**
 A view Controller that displays the time left of breaks and/or pomodoros.
 */
class TimeDisplayViewController: UIViewController {
    
    open var timeDisplay = TimeDisplay()
    var secondsLeft = Int()
    
    /**
     Ticks every second to update the seconds left property of the timerView.
     Does not keep track of the time since the beginning of the current session.
     */
    var sessionTimer: Timer?
    private var delayTimer: Timer?
    var timeToNextFiringWhenPaused = TimeInterval()
    
    // MARK: - Timer
    
    private func startTimer() {
        sessionTimer?.invalidate()
        sessionTimer = Timer.scheduledTimer(
            timeInterval: 1,
            target: self,
            selector: #selector(handleTimerTick),
            userInfo: nil,
            repeats: true)
    }
    
    @objc private func handleTimerTick() {
        self.secondsLeft -= 1
        
        if self.secondsLeft >= 0 {
            timeDisplay.setTimeLabelTexts(forSecondsLeft: secondsLeft)
        } else {
            handleSessionEnding()
        }
    }
    
    func startTimer(delay: TimeInterval) {
        delayTimer?.invalidate()
        
        delayTimer = Timer.scheduledTimer(withTimeInterval: delay, repeats: false) { (timer) in
            timer.invalidate()
            self.startTimer()
            self.secondsLeft -= 1
            self.timeDisplay.setTimeLabelTexts(forSecondsLeft: self.secondsLeft)
        }
    }
    
    func startSession(dateStarted: Date, secondsInSession: Int) {
        let elapsedTime = Date().timeIntervalSince(dateStarted)
        secondsLeft = secondsInSession - Int(ceil(elapsedTime))
        
        let delay = ceil(elapsedTime) - elapsedTime
        startTimer(delay: delay)
        
        timeDisplay.setTimeLabelTexts(forSecondsLeft: secondsLeft)
    }
    
    func pause(datePaused: Date) {
        // use delay timer in the case where pause was tapped before the previous delayTimer fired
        // timerFroDelay is the timer we will use to get the time from now until the next fire date
        if let timerForDelay = sessionTimer != nil && sessionTimer!.isValid ? sessionTimer : delayTimer {
            timeToNextFiringWhenPaused = timerForDelay.fireDate.timeIntervalSince(datePaused)
        }
        
        delayTimer?.invalidate()
        
        guard sessionTimer != nil && sessionTimer!.isValid else { return }
        sessionTimer?.invalidate()
        //        let timeSinceLastFiring = 1 - timeToNextFiring
        //        let timeElapsed = Date().timeIntervalSince(datePaused)
        //
        //        let wholeSecondsPassed = Int(floor(timeElapsed))
        //        let fractionalSecondsPassed = timeElapsed - floor(timeElapsed)
        //
        //        // if the timer was closer to firing than the fractional part of the time elapsed since the
        //        // user tapped the pause button, then we need to advance seconds passed (subtract from it)
        //        // by one additional second, not just the whole number of seconds elapsed.
        //        let secondMarkWasCrossed = fractionalSecondsPassed > timeSinceLastFiring
        //
        //        let secondsPassedDifference = wholeSecondsPassed + (secondMarkWasCrossed ? 1 : 0)
        //        secondsLeft -= secondsPassedDifference
        //
        //        timeDisplay.setTimeLabelTexts(forSecondsLeft: secondsLeft)
    }

    func resume(dateResumed: Date, secondsLeftBeforeResuming: Int, timeToNextFiringWhenPaused: TimeInterval) {
        delayTimer?.invalidate()
        self.timeToNextFiringWhenPaused = timeToNextFiringWhenPaused
        let timeIntervalLeftWhenResumed = timeToNextFiringWhenPaused + TimeInterval(secondsLeftBeforeResuming)
        let timeIntervalPassedSinceResumed = Date().timeIntervalSince(dateResumed)
        let timeIntervalLeft = timeIntervalLeftWhenResumed - timeIntervalPassedSinceResumed
        secondsLeft = Int(timeIntervalLeft)
        let delay = timeIntervalLeft - TimeInterval(Int(timeIntervalLeft))
        
        timeDisplay.setTimeLabelTexts(forSecondsLeft: secondsLeft)
        startTimer(delay: delay)
    }
    
    
    // MARK: Session
    
    func startPomodoro(dateStarted: Date) {
        startSession(dateStarted: dateStarted, secondsInSession: SavedPreferencesManager.shared.secondsInPomodoro)
    }
    
    func startBreak(dateStarted: Date) {
        startSession(dateStarted: dateStarted, secondsInSession: SavedPreferencesManager.shared.secondsInBreak)
    }
    
    open func handleSessionEnding() {
        delayTimer?.invalidate()
        sessionTimer?.invalidate()
        
        switch SessionStateManager.shared.state.type {
        case .pomodoro:
            getReadyForBreak()
        case .break:
            getReadyForPomodoro()
        }
    }
    
    func getReadyForPomodoro() {
        delayTimer?.invalidate()
        sessionTimer?.invalidate()
        secondsLeft = SavedPreferencesManager.shared.secondsInPomodoro
        
        timeDisplay.configureColorsForPomodoro()
        timeDisplay.setTimeLabelTexts(forSecondsLeft: secondsLeft)
    }
    
    func getReadyForBreak() {
        delayTimer?.invalidate()
        sessionTimer?.invalidate()
        secondsLeft = SavedPreferencesManager.shared.secondsInBreak
        
        timeDisplay.configureColorsForBreak()
        timeDisplay.setTimeLabelTexts(forSecondsLeft: secondsLeft)
    }
}
