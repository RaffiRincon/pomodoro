//
//  ManageTasksViewController.swift
//  Pomodoro
//
//  Created by Rafael Rincon on 8/16/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import UIKit
import CoreData

class ManageTasksViewController: UIViewController {
    
    private var taskTableView = UITableView()
    
    fileprivate var tasks = [Task]()
    
    // MARK: - Lifecycle Events
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationItem()
        configureTableView()
        configureButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadTaskList()
    }
    
    // MARK: - Configure Views
    
    private func configureButtons() {
        // configure startWithoutTaskButton
        
        let startWithoutTaskButton = UIButton.outlinedButton(
            width: K.Dimensions.StartWithNoTaskButton.width,
            height: K.Dimensions.StartWithNoTaskButton.height)
        startWithoutTaskButton.translatesAutoresizingMaskIntoConstraints = false
        
        startWithoutTaskButton.setAttributedTitle(K.AttributedString.StartWithNoTaskButton.attributedTitle, for: .normal)
        startWithoutTaskButton.titleLabel?.numberOfLines = 2
        startWithoutTaskButton.titleLabel?.textAlignment = .center
        
        view.addSubview(startWithoutTaskButton)
        NSLayoutConstraint.activate([
            startWithoutTaskButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -K.Dimensions.StartWithNoTaskButton.bottomSpacing),
            startWithoutTaskButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
            ])
        
        startWithoutTaskButton.addTarget(self, action: #selector(handleStartWithNoTaskButtonTap), for: .touchUpInside)
    }
    
    private func configureNavigationItem() {
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(
                barButtonSystemItem: .add,
                target: self,
                action: #selector(handleAddButtonTap))
        ]
        
        navigationItem.title = "Tasks"
    }
    
    private func configureTableView() {
        taskTableView.delegate = self
        taskTableView.dataSource = self
        
        taskTableView.separatorStyle = .none
        taskTableView.backgroundColor = K.Color.background
        
        taskTableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(taskTableView)
        NSLayoutConstraint.activate([
            taskTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            taskTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            taskTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            taskTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            ])
        
        taskTableView.contentInset = UIEdgeInsets(
            top: 0,
            left: 0,
            bottom: K.Dimensions.TaskTableView.bottomExtraSpace,
            right: 0)
    }
    
    // MARK: - Helper
    
    private func reloadTaskList() {
        let fetchRequest: NSFetchRequest<Task> = Task.fetchRequest()
        fetchRequest.predicate = NSPredicate(value: true)
        let sortByDate = NSSortDescriptor(key: "dateCreated", ascending: false)
        fetchRequest.sortDescriptors = [sortByDate]
        
        tasks = (try? AppDelegate.viewContext.fetch(fetchRequest)) ?? [Task]()
        taskTableView.reloadData()
    }
    
    // MARK: - Actions
    
    @objc private func handleAddButtonTap() {
        let addTaskController = UIAlertController(
            title: K.String.AlertController.AddTask.title,
            message: nil,
            preferredStyle: .alert)
        
        var taskTitleTextField: UITextField!
        
        // title text field
        addTaskController.addTextField { (textField) in
            taskTitleTextField = textField
            taskTitleTextField.placeholder = K.String.AlertController.AddTask.titlePlaceholder
            taskTitleTextField.keyboardType = UIKeyboardType.alphabet
            taskTitleTextField.borderStyle = .none
        }
        
        var numberOfPomodorosTextField: UITextField!
        
        // pomodoros text field
        addTaskController.addTextField { (textField) in
            numberOfPomodorosTextField = textField
            numberOfPomodorosTextField.placeholder = K.String.AlertController.AddTask.numberOfPomodorosPlaceholder
            numberOfPomodorosTextField.keyboardType = .asciiCapableNumberPad
            numberOfPomodorosTextField.borderStyle = .none
        }
        
        let createAction = UIAlertAction(title: K.String.AlertController.AddTask.rightButtonTitle, style: .default) { (action) in
            var errorMessage: String?
            
            if let numberOfPomodorosText = numberOfPomodorosTextField.text,
                numberOfPomodorosText.count > 0 {
                if let expectedNumberOfPomodoros = Int64(numberOfPomodorosText) {
                    if expectedNumberOfPomodoros > 0 {
                        // Default to a single space if there is no title, to lay views out correctly
                        var title = taskTitleTextField.text ?? " "
                        if title.count == 0 {
                            title = " "
                        }
                        
                        let newTask = Task(context: AppDelegate.viewContext)
                        newTask.dateCreated = Date()
                        newTask.expectedNumberOfPomodoros = expectedNumberOfPomodoros
                        newTask.title = title
                        self.reloadTaskList()
                    } else {
                        // handle no pomodoros
                        errorMessage = K.String.CouldNotCreateTaskController.noPomodorosMessage
                    }
                } else {
                    // handle unable to create pomodoros number
                    errorMessage = K.String.CouldNotCreateTaskController.unableToParsePomodoros
                }
            } else {
                // handle no pomodoro specified
                errorMessage = K.String.CouldNotCreateTaskController.noPomodorosMessage
            }
            
            if errorMessage != nil {
                self.presentOKCancelAlertController(
                    title: K.String.CouldNotCreateTaskController.title,
                    message: errorMessage!,
                    okAction: {
                    self.present(addTaskController, animated: true)
                })
            }
        }
        
        addTaskController.addAction(createAction)
        
        let cancelAction = UIAlertAction(
            title: K.String.AlertController.AddTask.leftButtonTitle,
            style: .cancel)     { (action) in
                addTaskController.dismiss(animated: true, completion: nil)
        }
        
        addTaskController.addAction(cancelAction)
        
        present(addTaskController, animated: true, completion: nil)
    }
    
    @objc private func handleStartWithNoTaskButtonTap() {
        let viewTaskViewController = ViewTaskViewController()
        
        navigationController?.pushViewController(viewTaskViewController, animated: true)
    }
}

// MARK: - Table View Data Source

extension ManageTasksViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let taskCellReuseId = K.String.TaskTableView.taskCellReuseId
        let cell = tableView.dequeueReusableCell(withIdentifier: taskCellReuseId)
            ?? UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: taskCellReuseId)
        cell.selectionStyle = .none
        cell.backgroundColor = K.Color.cellBackground
        
        guard indexPath.row < tasks.count else {
            cell.textLabel?.text = ""
            
            return cell
        }
        
        let taskForCell = tasks[indexPath.row]
        if let safeTextLabel = cell.textLabel, let safeDetailTextLabel = cell.detailTextLabel {
            safeTextLabel.textColor = .white
            safeDetailTextLabel.textColor = .white
            
            safeTextLabel.text = taskForCell.title
            safeDetailTextLabel.text = "Completed: \(taskForCell.numberOfPomodorosDone)/\(taskForCell.expectedNumberOfPomodoros)"
        }
        
        return cell
    }
}

// MARK: - Table View Delegate

extension ManageTasksViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = tasks[indexPath.row]
        
        let viewTaskViewController = ViewTaskViewController()
        viewTaskViewController.task = task
        
        navigationController?.pushViewController(viewTaskViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row < tasks.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            AppDelegate.viewContext.delete(tasks[indexPath.row])
            reloadTaskList()
        }
    }
}

