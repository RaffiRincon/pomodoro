//
//  Task.swift
//  Pomodoro
//
//  Created by Rafael Rincon on 8/17/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import UIKit
import CoreData

class Task: NSManagedObject {
    
    static func textFromTask(_ task: Task) -> String {
        return "Completed: \(task.numberOfPomodorosDone)/\(task.expectedNumberOfPomodoros)"
    }
}
