//
//  Constants.swift
//  Pomodoro
//
//  Created by Rafael Rincon on 8/16/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import Foundation
import UIKit

struct K {
    private init() {}
    
    struct String {
        private init() {}
        
        struct AdMob {
            static let appID = SensitiveConstants.AdMob.appID
            static let viewPomodoroBannerAdUnitID = SensitiveConstants.AdMob.viewPomodoroBannerAdUnitID
        }
        
        struct AlertController {
            struct AddTask {
                private init() {}
                
                static let title = "Add Task"
                static let titlePlaceholder = "Title (optional)"
                static let numberOfPomodorosPlaceholder = "Expected Pomodoros"
                
                static let rightButtonTitle = "Create"
                static let leftButtonTitle = "Cancel"
            }
            
            struct NotificationPermissionRequest {
                private init() {}
                
                static let title = "Allow Notifications"
                static let message = "Allow to see a countdown timers for breaks and Pomodoros."
                static let leftButtonTitle = "Cancel"
                static let middleButtonTitle = "Don't Ask Again"
                static let rightButtonTitle = "Allow"
            }
            
            struct GoIntoSettingsForNotifications {
                private init() {}
                
                static let title = "Allow Notifications"
                static let message = "Go into settings to change notification preferences."
                static let cancelButtonTitle = "Cancel"
                static let goButtonTitle = "Go"
            }
        }
        
        struct CouldNotCreateTaskController {
            private init() {}
            
            static let title = "Could Not Create Task"
            static let unableToParsePomodoros = "Unable to turn what you typed into a number. The maximum number of Pomodoros for a task is \(Int64.max)."
            static let noPomodorosMessage = "To create a task, please specify the number of Pomodoros you expect it will take."
        }
        
        struct TaskTableView {
            private init() {}
            
            static let taskCellReuseId = "taskCell"
        }
        
        struct UserDefaults {
            private init() {}
            
            static let minutesInPomodoroKey = "minutesInPomodoro"
            static let minutesInBreakKey = "minutesInBreak"
            
            static let doNotAskAgainKey = "doNotAskAgain"
        }
        
        struct SkipButton {
            private init() {}
            
            static let title = "Skip"
        }
        
        struct Notification {
            private init() {}
            
            static let pomodoroDone = "Pomodoro Done"
            static let breakDone = "Break Done"
            static let identifier = "notificationID"
            
            struct Pomodoro {
                private init() {}
                
                static let title = "Pomodoro"
            }
            
            struct Break {
                private init() {}
                
                static let title = "Break"
            }
            
            struct UserInfoKey {
                private init() {}
                
                static let event = "Action"
                static let category = "Category"
                static let secondsLeft = "SecondsLeft"
                static let date = "Date"
                static let timeToNextFiringWhenPaused = "timeToNextFiringWhenPaused"
            }
        }
    }
    
    struct Color {
        private init() {}
        
        static let primaryDark = UIColor(white: 66/255.0, alpha: 1)
        static let primaryLight = UIColor(white: 0.5, alpha: 1)
        static let accent = UIColor.with(red: 255, green: 221, blue: 0)
        static let background = primaryDark
        
        static let `break` = UIColor.with(red: 0, green: 122, blue: 255)
        static let pomodoro = UIColor.with(red: 255, green: 92, blue: 92)
        
        static let task = UIColor.with(red: 97, green: 250, blue: 96)
        static let done = task
        
        static let navigationBar = UIColor.white.withAlphaComponent(0.5)
        static let cellBackground = UIColor.clear
        
        struct Shadow {
            private init() {}
            
            static let alpha = Float(0.65)
        }
        
        struct Button {
            private init() {}
            
            static let lightBackground = Color.primaryLight
            static let outline = UIColor.white
        }
    }
    
    struct AttributedString {
        private init() {}
        
        struct StartWithNoTaskButton {
            private init() {}
            
            static let attributedTitle = generatedAttributedTitle()
            
            private static func generatedAttributedTitle() -> NSAttributedString {
                let title = "Start"
                let subtitle = "with no task"
                let fullTitle = "\(title)\n\(subtitle)"
                let subtitleLocation = title.count + 1
                
                let titleAttributes: [NSAttributedString.Key : Any] = [
                    NSAttributedString.Key.font : K.Font.StartWithNoTaskButton.titleFont,
                    NSAttributedString.Key.foregroundColor : UIColor.white
                ]
                let subtitleAttributes: [NSAttributedString.Key : Any] = [
                    NSAttributedString.Key.font : K.Font.StartWithNoTaskButton.subtitleFont,
                    NSAttributedString.Key.foregroundColor : UIColor.white
                ]
                
                let attributedFullTitle = NSMutableAttributedString(string: fullTitle, attributes: nil)
                attributedFullTitle.setAttributes(titleAttributes, range: NSRange(location: 0, length: title.count))
                attributedFullTitle.setAttributes(subtitleAttributes, range: NSRange(location: subtitleLocation, length: subtitle.count))
                
                return attributedFullTitle
            }
        }
    }
    
    // NOTE: add fonts to Info.plist as needed
    struct Font {
        private init() {}
        
        static let completedLabel = UIFont(name: "Roboto-Light", size: UIFont.systemFontSize)
        static let notificationDoneLabel = UIFont(name: "Roboto-Light", size: 24)!
        
        struct TimerLabel {
            private init() {}
            
            static let size = CGFloat(100)
            static let font = UIFont(name: "Roboto-Thin", size: size)!
        }
        
        struct StartWithNoTaskButton {
            private init() {}
            
            static let titleFont = UIFont(name: "Roboto-Light", size: 24)!
            static let subtitleFont = titleFont.withSize(12)
        }
        
        struct IconButton {
            private init() {}
            
            static let font = UIFont.systemFont(ofSize: 24)
        }
    }
    
    struct Spacing {
        private init() {}
        
        static let small = CGFloat(8)
        static let large = CGFloat(16)
        static let extraLarge = CGFloat(32)
    }
    
    struct Dimensions {
        private init() {}
        
        struct IconButton {
            private init() {}
            
            static let radius = CGFloat(35)
        }
        
        struct Notification {
            private init() {}
            static let height = TimerView.height + Spacing.large * 2
        }
        
        struct Shadow {
            private init() {}
            
            static let blurRadius = CGFloat(3)
        }
        
        struct SkipButton {
            private init() {}
            
            static let width = CGFloat(55)
            static let height = CGFloat(35)
        }
        
        struct StartPauseResumeButton {
            private init() {}
            
            static let width = CGFloat(65)
            static let height = CGFloat(35)
        }
        
        struct StartWithNoTaskButton {
            private init() {}
            
            static let height = CGFloat(55)
            static let width = CGFloat(110)
            static let bottomSpacing = K.Spacing.large
        }
        
        struct TaskTableView {
            private init() {}
            
            static let bottomExtraSpace = StartWithNoTaskButton.height + StartWithNoTaskButton.bottomSpacing * 2
        }
        
        struct TimerView {
            private init() {}
            
            static let numberLabelWidth = CGFloat(60)
            static let colonWidth = CGFloat(20)
            static let height = K.Font.TimerLabel.size + K.Spacing.small
            
            static let width = numberLabelWidth * 4 + colonWidth + K.Spacing.small * 2
        }
        
        struct ViewPomodoroBannerAd {
            private init() {}
            
            static let size = CGSize(width: 320, height: 50)
        }
    }
    
    struct Number {
        private init() {}
        
        struct Timer {
            private init() {}
            
            static let defaultPomodoroMinutes = Int(25)
            static let defaultBreakMinutes = Int(5)
        }
    }
}
