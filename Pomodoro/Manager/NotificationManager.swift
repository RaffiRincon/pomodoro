//
//  NotificationManager.swift
//  Pomodoro
//
//  Created by Rafael Rincon on 8/20/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import UIKit
import UserNotifications

enum SessionEvent: String {
    case start = "Start"
    case pause = "Pause"
    case resume = "Resume"
    case finish = "Finish"
}

enum SessionCategory: String {
    case pomodoro = "Pomodoro"
    case `break` = "Break"
    
    func title() -> String {
        switch self {
        case .pomodoro:
            return K.String.Notification.Pomodoro.title
        case .break:
            return K.String.Notification.Break.title
        }
    }
}

class NotificationManager: NSObject, UNUserNotificationCenterDelegate {
    
    private var minutesInPomodoro = SavedPreferencesManager.shared.minutesInPomodoro
    private var minutesInBreak = SavedPreferencesManager.shared.minutesInBreak
    
    public static let shared = NotificationManager()
    override private init() {
        super.init()
    }
    
    func removeAllNotifications() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    // MARK: - Helper
    
    /**
     Remove all notifications and add notification with the given parameters.
     */
    private func setNotification(date: Date?, secondsLeft: Int?, timeToNextFiringWhenPaused: TimeInterval? = nil, event: SessionEvent, category: SessionCategory) {
        removeAllNotifications()
        
        let content = UNMutableNotificationContent()
        content.categoryIdentifier = category.rawValue
        content.userInfo = self.userInfo(
            date: date,
            secondsLeft: secondsLeft,
            timeToNextFiringWhenPaused: timeToNextFiringWhenPaused,
            event: event,
            category: category)
        content.title = category.title()
        
        if event == .finish {
            content.sound = UNNotificationSound.default
        }
        
        let request = UNNotificationRequest(
            identifier: K.String.Notification.identifier,
            content: content,
            trigger: nil)
        
        UNUserNotificationCenter.current().add(request)
    }
    
    private func userInfo(date: Date?, secondsLeft: Int?, timeToNextFiringWhenPaused: TimeInterval? = nil, event: SessionEvent, category: SessionCategory) -> [String: Any] {
        var info: [String: Any] = [
            K.String.Notification.UserInfoKey.event: event.rawValue,
            K.String.Notification.UserInfoKey.category: category.rawValue
        ]
        
        if date != nil {
            info[K.String.Notification.UserInfoKey.date] = date
        }
        
        if secondsLeft != nil {
            info[K.String.Notification.UserInfoKey.secondsLeft] = secondsLeft
        }
        
        if timeToNextFiringWhenPaused != nil {
            info[K.String.Notification.UserInfoKey.timeToNextFiringWhenPaused] = timeToNextFiringWhenPaused
        }
        
        return info
    }
    
    // MARK: - Start Notifications
    
    func sendStartPomodoroNotification(dateStarted: Date) {
        setNotification(
            date: dateStarted,
            secondsLeft: SavedPreferencesManager.shared.minutesInPomodoro,
            event: .start,
            category: .pomodoro)
        schedulePomodoroDoneNotification(dateStarted: dateStarted)
    }
    
    func sendStartBreakNotification(dateStarted: Date) {
        setNotification(
            date: dateStarted,
            secondsLeft: SavedPreferencesManager.shared.minutesInBreak,
            event: .start,
            category: .break)
        scheduleBreakDoneNotification(dateStarted: dateStarted)
    }
    
    // MARK: - Pause Notifications
    
    func sendPausePomodoroNotification(secondsLeft: Int, datePaused: Date) {
        setNotification(
            date: datePaused,
            secondsLeft: secondsLeft,
            event: .pause,
            category: .pomodoro)
    }
    
    func sendPauseBreakNotification(secondsLeft: Int, datePaused: Date) {
        setNotification(
            date: datePaused,
            secondsLeft: secondsLeft,
            event: .pause,
            category: .break)
    }
    
    // MARK: - Resume Notifications
    
    func sendResumePomodoroNotification(dateResumed: Date, secondsLeft: Int, timeToNextFiringWhenPaused: TimeInterval) {
        setNotification(
            date: dateResumed,
            secondsLeft: secondsLeft,
            timeToNextFiringWhenPaused: timeToNextFiringWhenPaused,
            event: .resume,
            category: .pomodoro)
        
        let timeLeft = TimeInterval(secondsLeft) - Date().timeIntervalSince(dateResumed)
        scheduleDoneNotification(timeLeft: timeLeft, category: .pomodoro)
    }
    
    func sendResumeBreakNotification(dateResumed: Date, secondsLeft: Int, timeToNextFiringWhenPaused: TimeInterval) {
        setNotification(
            date: dateResumed,
            secondsLeft: secondsLeft,
            timeToNextFiringWhenPaused: timeToNextFiringWhenPaused,
            event: .resume,
            category: .break)
        
        let timeLeft = TimeInterval(secondsLeft) - Date().timeIntervalSince(dateResumed)
        scheduleDoneNotification(timeLeft: timeLeft, category: .break)
    }
    
    // MARK: - Done Notifications
    
    private func scheduleDoneNotification(timeLeft: TimeInterval, category: SessionCategory) {
        var notificationTitle: String!
        switch category {
        case .pomodoro:
            notificationTitle = K.String.Notification.pomodoroDone
        case .break:
            notificationTitle = K.String.Notification.breakDone
        }
        
        let content = UNMutableNotificationContent()
        content.sound = UNNotificationSound.default
        content.title = notificationTitle
        content.userInfo = self.userInfo(date: nil, secondsLeft: nil, event: .finish, category: category)
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeLeft, repeats: false)
        
        let notificationRequest = UNNotificationRequest(
            identifier: K.String.Notification.identifier,
            content: content,
            trigger: trigger)
        
        UNUserNotificationCenter.current().add(notificationRequest, withCompletionHandler: nil)
    }
    
    private func schedulePomodoroDoneNotification(dateStarted: Date) {
        let timeIntervalInPomodoro = TimeInterval(SavedPreferencesManager.shared.secondsInPomodoro)
        let timeLeft = timeIntervalInPomodoro - Date().timeIntervalSince(dateStarted)
        
        scheduleDoneNotification(timeLeft: timeLeft, category: .pomodoro)
    }
    
    private func scheduleBreakDoneNotification(dateStarted: Date) {
        let timeIntervalInPomodoro = TimeInterval(SavedPreferencesManager.shared.secondsInBreak)
        let timeLeft = timeIntervalInPomodoro - Date().timeIntervalSince(dateStarted)
        scheduleDoneNotification(timeLeft: timeLeft, category: .break)
    }
    
    // MARK: - UserNotificationCenter Delegate
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        let event = SessionEvent(rawValue: userInfo[K.String.Notification.UserInfoKey.event] as! String)!
        
        switch event {
        case .pause, .resume, .start:
            completionHandler([.alert])
        case .finish:
            completionHandler([.alert, .sound])
        }
    }
}
