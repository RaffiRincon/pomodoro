//
//  SavedPreferencesManager.swift
//  Pomodoro
//
//  Created by Rafael Rincon on 8/30/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import Foundation

class SavedPreferencesManager {
    private init() {}
    
    var minutesInPomodoro: Int {
        if let savedMinutesInPomodoro = UserDefaults().object(
            forKey: K.String.UserDefaults.minutesInPomodoroKey) as? Int {
            return savedMinutesInPomodoro
        } else {
            return K.Number.Timer.defaultPomodoroMinutes
        }
    }
    
    var secondsInPomodoro: Int {
        #if DEBUG
        return minutesInPomodoro * 60
        #else
        return minutesInPomodoro * 60
        #endif
    }
    
    var minutesInBreak: Int {
        if let savedMinutesInBreak = UserDefaults().object(
            forKey: K.String.UserDefaults.minutesInBreakKey) as? Int {
            return savedMinutesInBreak
        } else {
            return K.Number.Timer.defaultBreakMinutes
        }
    }
    
    var secondsInBreak: Int {
        #if DEBUG
        return minutesInBreak * 60
        #else
        return minutesInBreak * 60
        #endif
    }
    
    static let shared = SavedPreferencesManager()
}

