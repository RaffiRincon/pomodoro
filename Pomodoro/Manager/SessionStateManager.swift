//
//  TimerManager.swift
//  Pomodoro
//
//  Created by Rafael Rincon on 8/30/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import UIKit

enum SessionType {
    case `break`
    case pomodoro
}

enum SessionState {
    case breakRunning
    case breakPaused
    case breakReady
    case pomodoroRunning
    case pomodoroPaused
    case pomodoroReady
    
    var type: SessionType {
        switch self {
        case .breakReady, .breakRunning, .breakPaused:
            return .break
        case .pomodoroReady, .pomodoroRunning, .pomodoroPaused:
            return .pomodoro
        }
    }
}

class SessionStateManager {
    
    static var shared = SessionStateManager()
    private init() {}
    
    private(set) var previousState: SessionState = SessionState.breakRunning
    private(set) var state = SessionState.pomodoroReady {
        willSet(newValue) {
            previousState = state
        }
    }
    
    /**
     Prepares for the first session in the view controller lifecycle.
     */
    func prepareForFirstSession() {
        state = .pomodoroReady
    }
    
    func updateStateForStartPauseResume() {
        let previousState = state
        switch state {
        case .pomodoroRunning:
            state = .pomodoroPaused
        case .breakRunning:
            state = .breakPaused
        case .pomodoroPaused, .pomodoroReady:
            state = .pomodoroRunning
        case .breakPaused, .breakReady:
            state = .breakRunning
        }
        print("changed from \(previousState) to \(state)")
    }
    
    func updateStateForSkip() {
        switch state.type {
        case .pomodoro:
            state = .breakReady
        case .break:
            state = .pomodoroReady
        }
    }
    
    func updateStateForEndOfSession() {
        switch state.type {
        case .pomodoro:
            state = .breakReady
        case .break:
            state = .pomodoroReady
        }
    }
}
